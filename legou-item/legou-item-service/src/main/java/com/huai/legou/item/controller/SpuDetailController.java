package com.huai.legou.item.controller;

import com.huai.legou.item.po.SpuDetail;
import com.huai.legou.item.service.ISpuDetailService;
import com.lxs.legou.core.controller.BaseController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item/spuDetail")
@CrossOrigin
public class SpuDetailController extends BaseController<ISpuDetailService, SpuDetail> {
}
