package com.huai.legou.item.controller;

import com.huai.legou.item.po.SpecParam;
import com.huai.legou.item.service.ISpecParamService;
import com.lxs.legou.core.controller.BaseController;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/param")
@CrossOrigin
public class SpecParamController extends BaseController<ISpecParamService, SpecParam> {


    @ApiOperation(value = "查询",notes = "根据实体条件查询参数")
    @PostMapping (value = "/select-param-by-entity")
    public List<SpecParam> selectSpecParamApi(@RequestBody SpecParam entity){
        return service.list(entity);
    }
}
