package com.huai.legou.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huai.legou.item.po.SpecParam;
import com.huai.legou.item.service.ISpecParamService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecParamServiceImpl extends CrudServiceImpl<SpecParam> implements ISpecParamService {
    @Override
    public List<SpecParam> list(SpecParam entity) {
        QueryWrapper<SpecParam> query = Wrappers.<SpecParam>query();
        //根据分类id查询规格参数
        if (null != entity.getCid()){
            query.eq("cid_",entity.getCid());
        }
        if (null != entity.getSearching()){
            query.eq("searching_",entity.getSearching());
        }
        return getBaseMapper().selectList(query);
    }
}
