package com.huai.legou.item.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huai.legou.item.po.Sku;
import com.huai.legou.item.po.Spu;
import com.huai.legou.item.service.ISkuService;
import com.huai.legou.item.service.ISpuDetailService;
import com.huai.legou.item.service.ISpuService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpuServiceImpl extends CrudServiceImpl<Spu> implements ISpuService {

    @Autowired
    private ISpuDetailService spuDetailService;
    @Autowired
    private ISkuService skuService;
    @Override
    public void saveSpu(Spu spu) {
        //保存spu
        this.saveOrUpdate(spu);
        //保存spuDetail
        if (null == spu.getSpuDetail().getId()){
            spu.getSpuDetail().setId(spu.getId());
            spuDetailService.save(spu.getSpuDetail());
        }else {
            spuDetailService.updateById(spu.getSpuDetail());
        }
        //保存skus
        /**保存spu
         * 1：删除spu的所有的sku
         * 2：添加新的sku
         */
        skuService.remove(Wrappers.<Sku>query().eq("spu_id_",spu.getId()));
        for (Sku sku : spu.getSkus()) {
            sku.setSpuId(spu.getId());
            skuService.save(sku);
        }
    }
}
