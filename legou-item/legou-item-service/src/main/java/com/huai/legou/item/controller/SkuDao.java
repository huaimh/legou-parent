package com.huai.legou.item.controller;

import com.huai.legou.item.po.Sku;
import com.huai.legou.item.service.ISkuService;
import com.lxs.legou.core.controller.BaseController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item/sku")
@CrossOrigin

public class SkuDao extends BaseController<ISkuService, Sku> {
}
