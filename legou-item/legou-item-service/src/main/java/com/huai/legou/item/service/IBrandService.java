package com.huai.legou.item.service;

import com.huai.legou.item.po.Brand;
import com.huai.legou.item.po.Category;
import com.lxs.legou.core.service.ICrudService;

import java.util.List;

public interface IBrandService extends ICrudService<Brand> {
    /**
     * 根据商品id查询分类
     * @param id
     * @return
     */
    List<Category> selectCategoryByBrand(Long id);
}
