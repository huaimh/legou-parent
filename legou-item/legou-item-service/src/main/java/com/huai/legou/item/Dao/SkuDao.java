package com.huai.legou.item.Dao;

import com.huai.legou.item.po.Sku;
import com.lxs.legou.core.dao.ICrudDao;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SkuDao extends ICrudDao<Sku> {

    @Select("select * from sku_ where spu_id_ = #{skuId}")
    List<Sku> findBySkuId(Integer skuId);
}
