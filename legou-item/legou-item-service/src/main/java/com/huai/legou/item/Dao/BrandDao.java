package com.huai.legou.item.Dao;

import com.huai.legou.item.po.Brand;
import com.huai.legou.item.po.Category;
import com.lxs.legou.core.dao.ICrudDao;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface BrandDao extends ICrudDao<Brand> {
    /**
     * 删除商品和分类关联
     * @param id
     * @return
     */
    int deleteCategoryByBrand(Long id);
    /**
     * 关联商品和分类
     * @param categoryId
     * @param brandId
     * @return
     */
    int insertCategoryAndBrand(@Param("categoryId") Long categoryId, @Param("brandId") Long brandId);
    /**
     * 查询商品的分类
     * @param id
     * @return
     */
    List<Category> selectCategoryByBrand(Long id);
}
