package com.huai.legou.item.Dao;

import com.huai.legou.item.po.SpecGroup;
import com.lxs.legou.core.dao.ICrudDao;

import java.util.List;

public interface SpecGroupDao extends ICrudDao<SpecGroup> {
    /**
     * 根据实体条件动态查询分组
     * @param specGroup
     * @return
     */
    List<SpecGroup> selectList(SpecGroup specGroup);
}
