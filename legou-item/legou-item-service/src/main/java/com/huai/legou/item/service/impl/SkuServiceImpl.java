package com.huai.legou.item.service.impl;

import com.huai.legou.item.po.Sku;
import com.huai.legou.item.service.ISkuService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SkuServiceImpl extends CrudServiceImpl<Sku> implements ISkuService {
}
