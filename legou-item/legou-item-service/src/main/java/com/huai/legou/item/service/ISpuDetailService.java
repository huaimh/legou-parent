package com.huai.legou.item.service;

import com.huai.legou.item.po.SpuDetail;
import com.lxs.legou.core.service.ICrudService;

public interface ISpuDetailService extends ICrudService<SpuDetail> {
}
