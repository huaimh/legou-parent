package com.huai.legou.item.service.impl;

import com.huai.legou.item.Dao.BrandDao;
import com.huai.legou.item.po.Brand;
import com.huai.legou.item.po.Category;
import com.huai.legou.item.service.IBrandService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BrandServiceImpl extends CrudServiceImpl<Brand> implements IBrandService{

    @Override
    @Transactional(readOnly = false)
    public boolean saveOrUpdate(Brand entity) {
        boolean result = super.saveOrUpdate(entity);
        ((BrandDao) getBaseMapper()).deleteCategoryByBrand(entity.getId()); //删除商品和分类的关联
//添加商品和分类的关联
        Long[] roleIds = entity.getCategoryIds();
        if (null != roleIds) {
            for (Long roleId : roleIds) {
                ((BrandDao) getBaseMapper()).insertCategoryAndBrand(roleId, entity.getId());
            }
        }
        return result;
    }

    /**
     * 根据商品id查询分类
     *
     * @param id
     * @return
     */
    @Override
    public List<Category> selectCategoryByBrand(Long id) {
        return ((BrandDao) getBaseMapper()).selectCategoryByBrand(id);
    }
}
