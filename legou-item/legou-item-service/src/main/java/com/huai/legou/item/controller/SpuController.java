package com.huai.legou.item.controller;

import com.huai.legou.item.po.Spu;
import com.huai.legou.item.service.ISpuService;
import com.lxs.legou.core.controller.BaseController;
import com.lxs.legou.core.po.ResponseBean;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/item/spu")
@CrossOrigin
public class SpuController extends BaseController<ISpuService, Spu> {

    @PostMapping("/save-spu")
    public ResponseBean saveSpu(@RequestBody Spu spu)throws Exception{
        ResponseBean responseBean = new ResponseBean();

        try {
            service.saveSpu(spu);
        } catch (Exception e) {
            e.printStackTrace();
            responseBean.setMsg("保存失败");
            responseBean.setSuccess(false);
        }
        return responseBean;
    }

    @GetMapping("/list-all")
    public List<Spu> selectAll(){
        return service.list(new Spu());
    }

}
