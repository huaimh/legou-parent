package com.huai.legou.item.service;

import com.huai.legou.item.po.SpecParam;
import com.lxs.legou.core.service.ICrudService;

public interface ISpecParamService extends ICrudService<SpecParam> {
}
