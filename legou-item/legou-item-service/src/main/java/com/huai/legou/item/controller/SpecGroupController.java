package com.huai.legou.item.controller;

import com.huai.legou.item.po.SpecGroup;
import com.huai.legou.item.service.ISpecGroupService;
import com.lxs.legou.core.controller.BaseController;
import com.lxs.legou.core.po.ResponseBean;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/item/group")
@CrossOrigin
public class SpecGroupController extends BaseController<ISpecGroupService, SpecGroup> {

    public ResponseBean saveGroup(@RequestBody List<SpecGroup> specGroups){
        ResponseBean rb = new ResponseBean();
        try {
            if (specGroups != null && specGroups.size() > 0){
                service.saveGroup(specGroups.get(0).getCid(),specGroups);
            }
        } catch (Exception e) {
            e.printStackTrace();
            rb.setSuccess(false);
            rb.setMsg("保存失败");
        }
        return rb;
    }
}
