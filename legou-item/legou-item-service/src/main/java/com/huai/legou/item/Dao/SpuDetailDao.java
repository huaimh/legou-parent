package com.huai.legou.item.Dao;

import com.huai.legou.item.po.SpuDetail;
import com.lxs.legou.core.dao.ICrudDao;

public interface SpuDetailDao extends ICrudDao<SpuDetail> {
}
