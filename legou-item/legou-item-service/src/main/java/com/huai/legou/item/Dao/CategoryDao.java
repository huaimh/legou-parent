package com.huai.legou.item.Dao;

import com.huai.legou.item.po.Category;
import com.lxs.legou.core.dao.ICrudDao;

public interface CategoryDao extends ICrudDao<Category> {
}
