package com.huai.legou.item.Dao;

import com.huai.legou.item.po.SpecParam;
import com.lxs.legou.core.dao.ICrudDao;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SpecParamDao extends ICrudDao<SpecParam> {

    @Select("select * from spec_param_ where group_id_ = #{groupId}")
    List<SpecParam> findByGroupId(Integer groupId);
}
