package com.huai.legou.item.service;

import com.huai.legou.item.po.Spu;
import com.lxs.legou.core.service.ICrudService;

public interface ISpuService extends ICrudService<Spu> {
    void saveSpu(Spu spu);
}
