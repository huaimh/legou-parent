package com.huai.legou.item.service.impl;

import com.huai.legou.item.po.SpuDetail;
import com.huai.legou.item.service.ISpuDetailService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SpuDetailServiceImpl extends CrudServiceImpl<SpuDetail> implements ISpuDetailService {
}
