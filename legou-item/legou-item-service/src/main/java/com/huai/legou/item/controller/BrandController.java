package com.huai.legou.item.controller;

import com.huai.legou.item.po.Brand;
import com.huai.legou.item.po.Category;
import com.huai.legou.item.service.IBrandService;
import com.lxs.legou.core.controller.BaseController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin  //解决跨域问题
@RestController
@RequestMapping("/brand")
public class BrandController extends BaseController<IBrandService, Brand> {
    //得到品牌的分类集合
    /**
     * 模板方法：在加载后执行
     *
     * @param entity
     */
    @Override
    public void afterEdit(Brand entity) {
        List<Category> categories = service.selectCategoryByBrand(entity.getId());
        Long[] longs = new Long[categories.size()];
        for (int i = 0; i < categories.size(); i++) {
            longs[i] = categories.get(i).getId();
        }
        entity.setCategoryIds(longs);
    }


}
