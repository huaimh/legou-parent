package com.huai.legou.item.controller;

import com.huai.legou.item.po.Category;
import com.huai.legou.item.service.ICategoryService;
import com.lxs.legou.core.controller.BaseController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item/category")
@CrossOrigin
public class CategoryController extends BaseController<ICategoryService, Category> {


}
