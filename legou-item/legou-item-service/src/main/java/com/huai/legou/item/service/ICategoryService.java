package com.huai.legou.item.service;

import com.huai.legou.item.po.Category;
import com.lxs.legou.core.service.ICrudService;

public interface ICategoryService extends ICrudService<Category> {
}
