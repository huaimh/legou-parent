package com.huai.legou.item.service;

import com.huai.legou.item.po.SpecGroup;
import com.lxs.legou.core.service.ICrudService;

import java.util.List;

public interface ISpecGroupService extends ICrudService<SpecGroup> {

    /**
     * 根据前台传递的规格参数列表，保存规格参数
     * @param cid
     * @param groups
     */
    void saveGroup(Long cid, List<SpecGroup> groups);
}
