package com.huai.legou.item.Dao;

import com.huai.legou.item.po.Spu;
import com.lxs.legou.core.dao.ICrudDao;

public interface SpuDao extends ICrudDao<Spu> {

}
