package com.huai.legou.item.service;

import com.huai.legou.item.po.Sku;
import com.lxs.legou.core.service.ICrudService;

public interface ISkuService extends ICrudService<Sku> {
}
