package com.huai.legou.item.api;

import com.huai.legou.item.po.Category;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/item/category")
public interface CategoryApi {
    @RequestMapping(value = "/list")
    List<Category> list(Category category);
}
