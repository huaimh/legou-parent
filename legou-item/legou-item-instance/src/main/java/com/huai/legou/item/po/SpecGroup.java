package com.huai.legou.item.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lxs.legou.core.po.BaseEntity;
import lombok.Data;

import java.util.List;

@Data
@TableName("spec_group_")
@JsonIgnoreProperties(value = {"handler"}) // Jackson 序列化/反序列化器忽略掉被标记字段中名为 “handler” 的属性，不进行处理。
public class SpecGroup extends BaseEntity {

    @TableField("cid_")
    private Long cid;

    @TableField("cid_")
    private String name;

    @TableField("cid_")
    private List<SpecParam> params; //改组下的所有规格参数集合
}
